/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportSale;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportTotal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class ReportSaleDao implements Dao<ReportTotal> {

    @Override
    public ReportTotal get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportTotal> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportTotal save(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ReportTotal update(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(ReportTotal obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReportTotal> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ReportSaleM> getReportSaleByMounth() {
        {
            ArrayList<ReportSaleM> list = new ArrayList();
            String sql = "SELECT \n"
                    + "  CASE \n"
                    + "    WHEN strftime('%m', order_datetime) = '01' THEN 'January'\n"
                    + "    WHEN strftime('%m', order_datetime) = '02' THEN 'February'\n"
                    + "    WHEN strftime('%m', order_datetime) = '03' THEN 'March'\n"
                    + "    WHEN strftime('%m', order_datetime) = '04' THEN 'April'\n"
                    + "    WHEN strftime('%m', order_datetime) = '05' THEN 'May'\n"
                    + "    WHEN strftime('%m', order_datetime) = '06' THEN 'June'\n"
                    + "    WHEN strftime('%m', order_datetime) = '07' THEN 'July'\n"
                    + "    WHEN strftime('%m', order_datetime) = '08' THEN 'August'\n"
                    + "    WHEN strftime('%m', order_datetime) = '09' THEN 'September'\n"
                    + "    WHEN strftime('%m', order_datetime) = '10' THEN 'October'\n"
                    + "    WHEN strftime('%m', order_datetime) = '11' THEN 'November'\n"
                    + "    WHEN strftime('%m', order_datetime) = '12' THEN 'December'\n"
                    + "  END AS Month,\n"
                    + "  SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime\n"
                    + "GROUP BY Month\n"
                    + "ORDER BY strftime('%m', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleM obj = ReportSaleM.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleM> getReportSaleByMounth(String begin, String end) {
        {
            ArrayList<ReportSaleM> list = new ArrayList();
            String sql = "SELECT \n"
                    + "  CASE \n"
                    + "    WHEN strftime('%m', order_datetime) = '01' THEN 'January'\n"
                    + "    WHEN strftime('%m', order_datetime) = '02' THEN 'February'\n"
                    + "    WHEN strftime('%m', order_datetime) = '03' THEN 'March'\n"
                    + "    WHEN strftime('%m', order_datetime) = '04' THEN 'April'\n"
                    + "    WHEN strftime('%m', order_datetime) = '05' THEN 'May'\n"
                    + "    WHEN strftime('%m', order_datetime) = '06' THEN 'June'\n"
                    + "    WHEN strftime('%m', order_datetime) = '07' THEN 'July'\n"
                    + "    WHEN strftime('%m', order_datetime) = '08' THEN 'August'\n"
                    + "    WHEN strftime('%m', order_datetime) = '09' THEN 'September'\n"
                    + "    WHEN strftime('%m', order_datetime) = '10' THEN 'October'\n"
                    + "    WHEN strftime('%m', order_datetime) = '11' THEN 'November'\n"
                    + "    WHEN strftime('%m', order_datetime) = '12' THEN 'December'\n"
                    + "  END AS Month,\n"
                    + "  SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime >= ? AND order_datetime <= ?\n"
                    + "GROUP BY Month\n"
                    + "ORDER BY strftime('%m', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setString(1, begin);
                stmt.setString(2, end);
                ResultSet rs = stmt.executeQuery(); // ใช้ executeQuery() แทน stmt.executeQuery(sql);

                while (rs.next()) {
                    String month = rs.getString("Month");
                    double totalSum = rs.getDouble("total_sum");
                    ReportSaleM obj = new ReportSaleM(month, totalSum);
                    list.add(obj);
                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleD> getReportSaleByDay() {
        {
            ArrayList<ReportSaleD> list = new ArrayList();
            String sql = "SELECT DATE(order_datetime) as Day, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime >= '2023-05-15' AND order_datetime <= '2023-10-17'\n"
                    + "GROUP BY DATE(order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleD obj = ReportSaleD.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleW> getReportSaleByWeek() {
        {
            ArrayList<ReportSaleW> list = new ArrayList();
            String sql = "SELECT DATE(order_datetime, 'weekday 0', '-7 days') as WeekStart, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "WHERE order_datetime\n"
                    + "GROUP BY DATE(order_datetime, 'weekday 0', '-7 days');";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleW obj = ReportSaleW.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSaleH> getReportSaleByHour() {
        {
            ArrayList<ReportSaleH> list = new ArrayList();
            String sql = "SELECT strftime('%Y-%m-%d %H:00:00', order_datetime) as HourStart, SUM(total) as total_sum\n"
                    + "FROM orders\n"
                    + "GROUP BY strftime('%Y-%m-%d %H:00:00', order_datetime);";
            Connection conn = DatabaseHelper.getConnect();
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(sql);

                while (rs.next()) {
                    ReportSaleH obj = ReportSaleH.fromRS(rs);
                    list.add(obj);

                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return list;
        }
    }

    public List<ReportSale> getYearReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt GROUP BY strftime(\"%Y\", receipt_timestamp)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getSelectedYearReport(String selectedYear) {

        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", receipt_timestamp) as period,SUM(receipt_total) as total FROM receipt WHERE strftime(\"%Y\", receipt_timestamp) = " + "\"" + selectedYear + "\" " + "GROUP BY strftime(\"%m\", receipt_timestamp) ORDER BY total ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getPayrollMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%m\", check_time_in) as period"
                + "FROM check_time\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReportSale> getSelectedNameReport(String selectedName) {

        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT employee_name FROM employee WHERE * = " + "\"" + selectedName + "\" ";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
