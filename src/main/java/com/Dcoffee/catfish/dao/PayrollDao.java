/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.dao;

import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.OrderProduct;
import com.Dcoffee.catfish.model.Payroll;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Miso
 */
public class PayrollDao implements Dao<Payroll> {

    @Override
    public Payroll get(int id) {
        Payroll payroll = null;
        String sql = "SELECT * FROM payroll WHERE payroll_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                payroll = Payroll.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return payroll;
    }

    @Override
    public List<Payroll> getAll() {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM payroll";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Payroll payroll = Payroll.fromRS(rs);
                list.add(payroll);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    public List<Payroll> getAll(int id) {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM payroll WHERE payroll_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Payroll item = Payroll.fromRS(rs);
                list.add(item);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Payroll save(Payroll obj) {
        String sql = "INSERT INTO payroll (payroll_date,payroll_amount,employee_id)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, dateFormat.format(obj.getDate()));
            stmt.setDouble(2, obj.getAmount());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Payroll update(Payroll obj) {
        String sql = "UPDATE payroll"
                + " SET payroll_date = ?, payroll_amount = ?, employee_id = ?"
                + " WHERE payroll_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, dateFormat.format(obj.getDate()));
            stmt.setDouble(2, obj.getAmount());
            stmt.setInt(3, obj.getEmployeeId());
            stmt.setInt(4, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Payroll obj) {
        String sql = "DELETE FROM payroll WHERE payroll_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Payroll> getAll(String where, String order) {
        ArrayList<Payroll> list = new ArrayList();
        String sql = "SELECT * FROM payroll where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Payroll payroll = Payroll.fromRS(rs);
                list.add(payroll);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
