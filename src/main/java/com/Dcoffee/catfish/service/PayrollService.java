/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.service;

import com.Dcoffee.catfish.dao.PayrollDao;
import com.Dcoffee.catfish.model.Payroll;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Miso
 */
public class PayrollService {

   public static Payroll paySalary(Date date, double salary, int id) {
        PayrollDao payrollDao = new PayrollDao();
        Payroll payroll = new Payroll(date, salary, id);
        return payrollDao.save(payroll);
    }

    public List<Payroll> getPayrolls() {
        PayrollDao payrollDao = new PayrollDao();
        return payrollDao.getAll();
    }

   

    

}
