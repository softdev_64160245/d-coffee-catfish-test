/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.crypto.Data;

/**
 *
 * @author User
 */
public class BillDetail {
    private int id;
    private String materialName;
    private int billDetailTotailprice;
    private int billDetailPriceperunit;
    private int billDetailQty;
    private int billId;
    private int materailId;

    public BillDetail(int id, String materialName, int billDetailTotailprice, int billDetailPriceperunit, int billDetailQty, int billId, int materailId) {
        this.id = id;
        this.materialName = materialName;
        this.billDetailTotailprice = billDetailTotailprice;
        this.billDetailPriceperunit = billDetailPriceperunit;
        this.billDetailQty = billDetailQty;
        this.billId = billId;
        this.materailId = materailId;
    }
    
    public BillDetail(String materialName, int billDetailTotailprice, int billDetailPriceperunit, int billDetailQty, int billId, int materailId) {
        this.id = -1;
        this.materialName = materialName;
        this.billDetailTotailprice = billDetailTotailprice;
        this.billDetailPriceperunit = billDetailPriceperunit;
        this.billDetailQty = billDetailQty;
        this.billId = billId;
        this.materailId = materailId;
    }
    
    public BillDetail() {
        this.id = -1;
        this.materialName = "";
        this.billDetailTotailprice = 0;
        this.billDetailPriceperunit = 0;
        this.billDetailQty = 0;
        this.billId = 0;
        this.materailId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public int getBillDetailTotailprice() {
        return billDetailTotailprice;
    }

    public void setBillDetailTotailprice(int billDetailTotailprice) {
        this.billDetailTotailprice = billDetailTotailprice;
    }

    public int getBillDetailPriceperunit() {
        return billDetailPriceperunit;
    }

    public void setBillDetailPriceperunit(int billDetailPriceperunit) {
        this.billDetailPriceperunit = billDetailPriceperunit;
    }

    public int getBillDetailQty() {
        return billDetailQty;
    }

    public void setBillDetailQty(int billDetailQty) {
        this.billDetailQty = billDetailQty;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getMaterailId() {
        return materailId;
    }

    public void setMaterailId(int materailId) {
        this.materailId = materailId;
    }

    @Override
    public String toString() {
        return "BillDetail{" + "id=" + id + ", materialName=" + materialName + ", billDetailTotailprice=" + billDetailTotailprice + ", billDetailPriceperunit=" + billDetailPriceperunit + ", billDetailQty=" + billDetailQty + ", billId=" + billId + ", materailId=" + materailId + '}';
    }
    
    public static BillDetail fromRS(ResultSet rs) {
        BillDetail billDetail = new BillDetail();
        try {
            billDetail.setId(rs.getInt("bill_detail_id"));
            billDetail.setMaterialName(rs.getString("materail_name"));
            billDetail.setBillDetailTotailprice(rs.getInt("bill_detail_totailprice"));
            billDetail.setBillDetailPriceperunit(rs.getInt("bill_detail_priceperunit"));
            billDetail.setBillDetailQty(rs.getInt("bill_detail_qty"));
            billDetail.setBillId(rs.getInt("bill_id"));
            billDetail.setMaterailId(rs.getInt("materail_id"));
        } catch (SQLException ex) {
            Logger.getLogger(BillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billDetail;
    }
}
