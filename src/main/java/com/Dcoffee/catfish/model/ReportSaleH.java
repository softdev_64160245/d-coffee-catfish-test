/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Miso
 */
public class ReportSaleH {
    private String hourStart;
    private double totalSum;
    
public ReportSaleH(String hourStart,double totalSum) {
        this.hourStart = hourStart;
        this.totalSum = totalSum;
    }
    public ReportSaleH() {
        this("",0);
    }

    public String getDay() {
        return hourStart;
    }

    public void setDay(String hourStart) {
        this.hourStart = hourStart;
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    @Override
    public String toString() {
        return "ReportSaleD{" + "hourStart=" + hourStart + ", totalSum=" + totalSum + '}';
    }
    public static ReportSaleH fromRS(ResultSet rs) {
        ReportSaleH obj = new ReportSaleH();
        try {
            
            obj.setDay(rs.getString("HourStart"));
            obj.setTotalSum(rs.getInt("total_sum"));

        } catch (SQLException ex) {
            Logger.getLogger(ReportSaleH.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;
    } 
    
}
