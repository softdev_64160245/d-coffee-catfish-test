/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.Dcoffee.catfish.test;

import com.Dcoffee.catfish.dao.CustomerDao;
import com.Dcoffee.catfish.helper.DatabaseHelper;
import com.Dcoffee.catfish.model.Customer;


/**
 *
 * @author Miso
 */
public class TestCustomerDao {

    public static void main(String[] args) {
        CustomerDao customerDao = new CustomerDao();
        for (Customer u : customerDao.getAll()) {
            System.out.println(u);
        }
        System.out.println("________________________________");
        //getone
        Customer cus1 = customerDao.get(1);
        System.out.println(cus1);
        System.out.println("________________________________");
        for (Customer u : customerDao.getAll(" cus_id like '1%' ")) {
            System.out.println(u);
        }

        DatabaseHelper.close();
    }
}
