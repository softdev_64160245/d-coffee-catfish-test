/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.Dcoffee.catfish.ui.Report;

import com.Dcoffee.catfish.model.ReportProduct;
import com.Dcoffee.catfish.model.ReportSaleD;
import com.Dcoffee.catfish.model.ReportSaleH;
import com.Dcoffee.catfish.model.ReportSaleM;
import com.Dcoffee.catfish.model.ReportSaleW;
import com.Dcoffee.catfish.model.ReportStock;
import com.Dcoffee.catfish.model.ReportTotalCustomer;
import com.Dcoffee.catfish.service.ReportService;
import com.Dcoffee.catfish.ui.DateLabelFormatter;
import java.awt.Color;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.ImageIcon;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author Miso
 */
public class ReportPanel extends javax.swing.JPanel {

    private final ReportService reportService;
    private UtilDateModel model1;
    private UtilDateModel model2;
    private List<ReportTotalCustomer> reportList;
    private List<ReportProduct> reportList1;
    private List<ReportStock> reportList2;
    private List<ReportSaleM> reportList3;
    private List<ReportSaleD> reportList4;
    private List<ReportSaleW> reportList5;
    private List<ReportSaleH> reportList6;
    private DefaultPieDataset Piedataset;

    /**
     * Creates new form ReportPanel
     */
    public ReportPanel() {
        initComponents();
        scrChart.setViewportView(new ReportLinePanel());
        btnSearch.setIcon(new ImageIcon("img/search.png"));
        btnPie.setIcon(new ImageIcon("img/piechart.png"));
        btnBar.setIcon(new ImageIcon("img/bargraph.png"));
        iconOrder.setIcon(new ImageIcon("img/Mcoffee.png"));
        iconMember.setIcon(new ImageIcon("img/mem.png"));
        iconStock.setIcon(new ImageIcon("img/outstock.png"));
        iconBest.setIcon(new ImageIcon("img/best.png"));
        reportService = new ReportService();
        reportList1 = reportService.getBestProduct();
        reportList = reportService.getTotalByCus();
        reportList2 = reportService.getOutStock();
        reportList3 = reportService.getReportSaleByMounth();
        reportList4 = reportService.getReportSaleByDay();
        reportList5 = reportService.getReportSaleByWeek();
        reportList6 = reportService.getReportSaleByHour();
        Search1();
        Search2();
//        System.out.println(""+reportList1);
        tblProduct.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblStock.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblCus.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleW.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleM.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleH.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        tblSaleD.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 14));
        initTableMember();
        initTableProduct();
        initTableStock();
        initTableSaleM();
        initTableSaleD();
        initTableSaleW();
        initTableSaleH();
//        initPirChart();
//        loadPieBestSeller();

    }

    private void Search1() {
        model1 = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.mouth", "Mouth");
        p.put("text.year", "Year");
        JDatePanelImpl dataPanel = new JDatePanelImpl(model1, p);
        JDatePickerImpl dataPicker = new JDatePickerImpl(dataPanel, new DateLabelFormatter());
        pnlSearch1.add(dataPicker);
        model1.setSelected(true);
    }
     private void Search2() {
        model2 = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.mouth", "Mouth");
        p.put("text.year", "Year");
        JDatePanelImpl dataPanel = new JDatePanelImpl(model2, p);
        JDatePickerImpl dataPicker = new JDatePickerImpl(dataPanel, new DateLabelFormatter());
        pnlSearch2.add(dataPicker);
        model2.setSelected(true);
    }

//    private void initPirChart() {
//        Piedataset = new DefaultPieDataset();
//        JFreeChart chart = ChartFactory.createPieChart("Best Seller", // chart title
//                Piedataset, // data
//                true, // include legend
//                true,
//                false);
//        ChartPanel chartPanel = new ChartPanel(chart);
//        pnlPie.add(chartPanel);
//    }
//
//    private void loadPieBestSeller() {
//        Piedataset.clear();
//        for (ReportProduct a : reportList1) {
//            Piedataset.setValue(a.getName(), a.getTotalAmount());
//        }
//    }
    private void initTableProduct() {
        tblProduct.setModel(new AbstractTableModel() {
            String[] columnNames = {"Product", "ToTal"};

            @Override

            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList1.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportProduct report = reportList1.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getName();
                    case 1:
                        return report.getTotalAmount();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableMember() {
        tblCus.setModel(new AbstractTableModel() {
            String[] columnNames = {"CustomerId", "CustomerName", "TotalPurchases", "ToTalOrders"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportTotalCustomer report = reportList.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getId();
                    case 1:
                        return report.getCusName() + " " + report.getSurName();
                    case 2:
                        return report.getTotalPurchases();
                    case 3:
                        return report.getTatalOrder();
                    default:
                        return "";
                }
            }
        });
//        System.out.println(""+reportList);
//        imgdash.setIcon(new ImageIcon("img/imgdash.png"));
    }

    private void initTableStock() {
        tblStock.setModel(new AbstractTableModel() {
            String[] columnNames = {"MaterialName", "MaterialQty", "MaterialMin"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList2.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportStock report = reportList2.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getMaterialName();
                    case 1:
                        // กำหนดสีเป็นสีแดงสำหรับคอลัมน์ที่ 2 (MaterialQty)
                        cellRenderer.setForeground(Color.RED);
                        return report.getMaterialQty();
                    case 2:
                        return report.getMaterialMin();
                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleM() {
        tblSaleM.setModel(new AbstractTableModel() {
            String[] columnNames = {"Mounth", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList3.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleM report = reportList3.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getMounth();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleD() {
        tblSaleD.setModel(new AbstractTableModel() {
            String[] columnNames = {"Day", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList4.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleD report = reportList4.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getDay();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleW() {
        tblSaleW.setModel(new AbstractTableModel() {
            String[] columnNames = {"WeekStart", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList5.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleW report = reportList5.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getWeekStart();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    private void initTableSaleH() {
        tblSaleH.setModel(new AbstractTableModel() {
            String[] columnNames = {"HourStart", "Total"};
            DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return reportList6.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReportSaleH report = reportList6.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return report.getDay();
                    case 1:
                        cellRenderer.setForeground(Color.RED);
                        return report.getTotalSum();

                    default:
                        return "";
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        iconOrder = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblSaleH = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblSaleM = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblSaleD = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblSaleW = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        iconMember = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCus = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        iconBest = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblProduct = new javax.swing.JTable();
        btnPie = new javax.swing.JButton();
        btnBar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        scrChart = new javax.swing.JScrollPane();
        plChart = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        iconStock = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblStock = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        pnlSearch2 = new javax.swing.JPanel();
        pnlSearch1 = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(65, 55, 54));

        jPanel1.setBackground(new java.awt.Color(156, 131, 112));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel1.add(iconOrder, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 71, 70));

        jLabel5.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Report Date");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 3, -1, -1));

        jLabel10.setForeground(new java.awt.Color(0, 0, 0));
        jLabel10.setText("______________________________________");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 43, 220, -1));

        tblSaleH.setBackground(new java.awt.Color(156, 131, 112));
        tblSaleH.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleH.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane7.setViewportView(tblSaleH);

        jPanel1.add(jScrollPane7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 180, 320, 140));

        tblSaleM.setBackground(new java.awt.Color(156, 131, 112));
        tblSaleM.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleM.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane4.setViewportView(tblSaleM);

        jPanel1.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 180, 280, 140));

        tblSaleD.setBackground(new java.awt.Color(156, 131, 112));
        tblSaleD.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleD.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane5.setViewportView(tblSaleD);

        jPanel1.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 320, 110));

        tblSaleW.setBackground(new java.awt.Color(156, 131, 112));
        tblSaleW.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblSaleW.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane6.setViewportView(tblSaleW);

        jPanel1.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 70, 280, 110));

        jPanel2.setBackground(new java.awt.Color(211, 197, 186));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel2.setForeground(new java.awt.Color(211, 197, 186));

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Regular Customer");

        iconMember.setForeground(new java.awt.Color(102, 102, 102));
        iconMember.setText("icon");

        tblCus.setBackground(new java.awt.Color(211, 197, 186));
        tblCus.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblCus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "CustomerId", "CustomerName", "TotalPurchases", "ToTalOrders"
            }
        ));
        jScrollPane1.setViewportView(tblCus);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(iconMember, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(iconMember, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(211, 197, 186));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanel3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        iconBest.setForeground(new java.awt.Color(0, 0, 0));
        iconBest.setText("icon");
        jPanel3.add(iconBest, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 50, 50));

        tblProduct.setBackground(new java.awt.Color(211, 197, 186));
        tblProduct.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblProduct.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jScrollPane2.setViewportView(tblProduct);

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 260, 170));

        btnPie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPieActionPerformed(evt);
            }
        });
        jPanel3.add(btnPie, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 130, 40));

        btnBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBarActionPerformed(evt);
            }
        });
        jPanel3.add(btnBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 250, 130, 40));

        jLabel6.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Best Seller");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 0, -1, -1));

        jLabel9.setForeground(new java.awt.Color(0, 0, 0));
        jLabel9.setText("______________________________________");
        jPanel3.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, 210, -1));

        plChart.setBackground(new java.awt.Color(156, 131, 112));
        plChart.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        plChart.setMaximumSize(new java.awt.Dimension(560, 367));
        plChart.setMinimumSize(new java.awt.Dimension(560, 367));

        javax.swing.GroupLayout plChartLayout = new javax.swing.GroupLayout(plChart);
        plChart.setLayout(plChartLayout);
        plChartLayout.setHorizontalGroup(
            plChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 985, Short.MAX_VALUE)
        );
        plChartLayout.setVerticalGroup(
            plChartLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 980, Short.MAX_VALUE)
        );

        scrChart.setViewportView(plChart);

        jPanel4.setBackground(new java.awt.Color(211, 197, 186));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));

        jLabel3.setFont(new java.awt.Font("AngsanaUPC", 2, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Out Stock !");

        tblStock.setBackground(new java.awt.Color(211, 197, 186));
        tblStock.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tblStock.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblStock);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(iconStock, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 327, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(iconStock, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(65, 55, 54));
        jPanel6.setForeground(new java.awt.Color(65, 55, 54));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlSearch2.setBackground(new java.awt.Color(65, 55, 54));
        jPanel6.add(pnlSearch2, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 10, 200, 33));

        pnlSearch1.setBackground(new java.awt.Color(65, 55, 54));
        jPanel6.add(pnlSearch1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, 200, 33));

        btnSearch.setBackground(new java.awt.Color(84, 70, 63));
        btnSearch.setFont(new java.awt.Font("AngsanaUPC", 1, 24)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(255, 255, 255));
        btnSearch.setText("Search");
        btnSearch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });
        jPanel6.add(btnSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 10, -1, 33));

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("_________________________________________________________________________________________________________________");
        jPanel6.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 590, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(scrChart, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(2, 2, 2)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrChart, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPieActionPerformed
        scrChart.setViewportView(new ReportPiePanel());

    }//GEN-LAST:event_btnPieActionPerformed

    private void btnBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBarActionPerformed
        scrChart.setViewportView(new ReportBarPanel());
    }//GEN-LAST:event_btnBarActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
String pattern = "yyyy-MM-dd";
        SimpleDateFormat formater = new SimpleDateFormat(pattern);
        System.out.println("" + formater.format(model1.getValue()) + " " + formater.format(model2.getValue()));
        String begin = formater.format(model1.getValue());
        String end = formater.format(model2.getValue());
        reportList3 = reportService.getReportSaleByMounth(begin, end);
        ((AbstractTableModel) tblSaleM.getModel()).fireTableDataChanged();
        scrChart.setViewportView(new ReportLinePanel());
        


    }//GEN-LAST:event_btnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBar;
    private javax.swing.JButton btnPie;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel iconBest;
    private javax.swing.JLabel iconMember;
    private javax.swing.JLabel iconOrder;
    private javax.swing.JLabel iconStock;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JPanel plChart;
    private javax.swing.JPanel pnlSearch1;
    private javax.swing.JPanel pnlSearch2;
    private javax.swing.JScrollPane scrChart;
    private javax.swing.JTable tblCus;
    private javax.swing.JTable tblProduct;
    private javax.swing.JTable tblSaleD;
    private javax.swing.JTable tblSaleH;
    private javax.swing.JTable tblSaleM;
    private javax.swing.JTable tblSaleW;
    private javax.swing.JTable tblStock;
    // End of variables declaration//GEN-END:variables
}
